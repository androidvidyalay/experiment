const BigNumber = require('bignumber.js')
module.exports = (function () {
  let util = {}
  //const singleUnit = 1e9;
  const decimals = 1e18;
  util.contractAddress = '0x2DBAb7009a77eB7AdF3ad981dAeE7ab1D7bb9f56'
  
  util.cardsContractAddress = '0x108b1dfa7ce0888524Be3bB4275242bE6e93A215'
  util.walletAddress = '';
  util.chainId = 4002;
  
  util.handleError = function (snack, e) {
    snack.display = true
    snack.text = e
    snack.color = "error"
  }
  util.handleInfo = function (snack, e) {
    snack.display = true
    snack.text = e
    snack.color = "info"
  }
  util.handleSuccess = function (snack, e) {
    snack.display = true
    snack.text = e
    snack.color = "success"
  }
  util.getPayableValue = function(amount){
    const val = new BigNumber(decimals).times(amount);
    return val.toString();//.integerValue(BigNumber.ROUND_DOWN).toFixed();
  }
  util.formatDate = function(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    let day = date.getDate();
    day = day < 10 ? "0" + day : day;
    let month = date.getMonth() + 1;
    month = month < 10 ? "0" + month : month;

    return date.getFullYear() + "-" + month + "-"  + day + "  " + strTime;
  }
  util.toBN = function(number, decimalPlaces) {
    const multiplied = BigNumber(number).shiftedBy(decimalPlaces)
    return multiplied.integerValue(BigNumber.ROUND_DOWN)
  }
  util.toBNFixed = function(number) {
    const multiplied = BigNumber(number).shiftedBy(decimals)
    return multiplied.integerValue(BigNumber.ROUND_DOWN).toFixed()
  }

  return util
}())

