const Web3 = require('web3');
const address = '0x11F91A5Dd67efD2d8Ca1BD79fd06ED49cd25e7C1'
let Option
class Web3Client{
  constructor(){
    this.contract = {}
  }
  async init(){
    const abi = [{"inputs":[{"internalType":"address payable","name":"owner_","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"tokenAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"quantity","type":"uint256"}],"name":"Created","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"identifier","type":"bytes32"},{"indexed":false,"internalType":"enum Wallet.Result","name":"result","type":"uint8"},{"indexed":false,"internalType":"uint256","name":"blockTimestamp","type":"uint256"}],"name":"OptionResolved","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"address","name":"tokenAddress","type":"address"},{"indexed":false,"internalType":"enum Wallet.Result","name":"tokenResult","type":"uint8"},{"indexed":false,"internalType":"enum Wallet.Result","name":"optionResult","type":"uint8"}],"name":"Redeem","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"address","name":"tokenAddress","type":"address"},{"indexed":false,"internalType":"uint256","name":"quantity","type":"uint256"}],"name":"Redeemed","type":"event"},{"inputs":[{"internalType":"bytes32","name":"identifier","type":"bytes32"},{"internalType":"uint256","name":"units","type":"uint256"}],"name":"buyOption","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"string","name":"instrument","type":"string"},{"internalType":"uint256","name":"strikePrice","type":"uint256"},{"internalType":"enum Wallet.Operator","name":"operator","type":"uint8"},{"internalType":"uint256","name":"units","type":"uint256"},{"internalType":"uint256","name":"redeemPrice","type":"uint256"},{"internalType":"uint256","name":"expiry","type":"uint256"}],"name":"createOption","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"deposit","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"deposits","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"name":"noContracts","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"name":"options","outputs":[{"internalType":"bytes32","name":"identifier","type":"bytes32"},{"internalType":"string","name":"instrument","type":"string"},{"internalType":"string","name":"symbol","type":"string"},{"internalType":"uint256","name":"strikePrice","type":"uint256"},{"internalType":"enum Wallet.Operator","name":"operator","type":"uint8"},{"internalType":"uint256","name":"redeemPrice","type":"uint256"},{"internalType":"uint256","name":"expiry","type":"uint256"},{"internalType":"enum Wallet.Result","name":"result","type":"uint8"},{"internalType":"bool","name":"resolved","type":"bool"},{"internalType":"address","name":"seller","type":"address"},{"internalType":"uint256","name":"resolvedPrice","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"optionsArray","outputs":[{"internalType":"bytes32","name":"identifier","type":"bytes32"},{"internalType":"string","name":"instrument","type":"string"},{"internalType":"string","name":"symbol","type":"string"},{"internalType":"uint256","name":"strikePrice","type":"uint256"},{"internalType":"enum Wallet.Operator","name":"operator","type":"uint8"},{"internalType":"uint256","name":"redeemPrice","type":"uint256"},{"internalType":"uint256","name":"expiry","type":"uint256"},{"internalType":"enum Wallet.Result","name":"result","type":"uint8"},{"internalType":"bool","name":"resolved","type":"bool"},{"internalType":"address","name":"seller","type":"address"},{"internalType":"uint256","name":"resolvedPrice","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"optionsCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address payable","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"}],"name":"redeem","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"identifier","type":"bytes32"},{"internalType":"uint256","name":"resolutionPrice","type":"uint256"}],"name":"resolveOption","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"testVar","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"withdraw","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"withdrawAll","outputs":[{"internalType":"bool","name":"success","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"name":"yesContracts","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"}]
    
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.enable();
      console.log(Web3)
      this.contract = new window.web3.eth.Contract(abi, address);
      console.log(this.contract)
      const owner = await this.contract.methods.owner().call();
      console.log(owner)

      const optionsCount = await this.contract.methods.optionsCount().call();
      console.log(optionsCount)
    }
  }
  async getOption(optionIdx){
    Option = await this.contract.methods.optionsArray(optionIdx).call();
    console.log(Option)
    return {identifierShort: Option.identifier.substring(0,8)+"...", identifier: Option.identifier, optionCode: Option.symbol, resolved: Option.resolved, expiry: Option.expiry}
  }
  //Highly inefficient, should be single call
  async getOptions(){
    const optionsArray = []
    const count = await this.getOptionsCount()
    for (let i=0; i < count; i++){
      const binaryOption = await this.getOption(i)
      optionsArray.push(binaryOption)
    }
    return optionsArray
  }
  async getOptionsCount(){
    return await this.contract.methods.optionsCount().call();
  }
  async getCurrentAccount() {
    const accounts = await window.web3.eth.getAccounts();
    return accounts[0];
}
  async buyOption(optionId, val){
    const account = await this.getCurrentAccount();
      await this.contract.methods.buyOption(optionId, 1).send({from: account, value: val})
  }
}
module.exports = {Web3Client}