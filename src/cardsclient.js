const ethers = require('ethers');
const util = require('./util');
const BigNumber = require("bignumber.js");
const abi = require('./cards.json');
const { soliditySha3 } = require("web3-utils")

let provider
const suits = {1: 'c', 2: 'h', 3: 'd', 4: 's'}
const ranks = {1: 'A', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: 'T', 11: 'J', 12: 'Q', 13: 'K'}
const gameStates = {0: 'NOT_CREATED', 1: 'CREATED', 2: 'STARTED', 3: 'ASKED_FOR_SHOW', 4: 'REVEALED', 5: 'WINNER_COMPUTED', 6: 'ALL_FOLDED_BUT_ONE', 7: 'PAID_OUT'}
      
class CardsClient {
    constructor() {
        this.contract = {}
        this.walletAddr = ''
        this.game = {}
    }
    getSuits(){
        return suits;        
    }
    getRanks(){
        return ranks;
    }
    async getCurrentAccount(){
        const accounts = await provider.listAccounts();
        return accounts.length > 0 ?  accounts[0] : ''
    }
    async init() {
        provider = new ethers.providers.Web3Provider(window.ethereum);
        const network = await provider.getNetwork()
        if (network.chainId == util.chainId) await this.initContract();
        return network.chainId;
    }
    async initContract() {
        this.contract = new ethers.Contract(util.cardsContractAddress, abi, provider);
    }
    async getGames(creator, dateNum, openGames, closedGames){
        const len = await this.contract.getGamesLength(creator, dateNum);
        for (let i=0; i<len; i++){
            const game = await this.retrieveGame(creator, dateNum, i);
            console.log(game)
            console.log(game.state)
            console.log(Object.values(gameStates).indexOf('PAID_OUT'))
            if (game.state == Object.values(gameStates).indexOf('PAID_OUT'))
                closedGames.push(game);
            else openGames.push(game)
        }
    }
    async retrieveGame(creator, dateNum, gameId){
        const gameData = await this.contract.retrieveGame(creator, dateNum, gameId);
        return {
            id: gameId,
            creator,
            dateNum,
            commissionDisplay: gameData.commission/100,
            nextPlayer: gameData.nextTurn == 0x0000000000000000000000000000000000000000 ? '' : gameData.nextTurn,
            gameWinner: gameData.winner == 0x0000000000000000000000000000000000000000 ? '' : gameData.winner,
            totalPotFormatted : new BigNumber(gameData.totalPot.toString()).shiftedBy(-18),
            perRoundWager : new BigNumber(gameData.eachRoundBet.toString()).shiftedBy(-18),
            ...gameData
        }
    }
    async getRegistration(currentAccount){
        const withSigner = this.contract.connect(provider.getSigner());
        return await withSigner.getUserRegistration(currentAccount);
    }
    async createGame(eachRoundBet, creatorCommission, forceTimeout, datenum, tag){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.createGame(util.getPayableValue(eachRoundBet), creatorCommission, forceTimeout, datenum, tag);
    }
    async joinGame(creator, datenum, id, seedPhrase, eachRoundBet){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.joinGame(creator, datenum, id, seedPhrase, {value: util.getPayableValue(eachRoundBet*2)});
    }
    async playTurn(creator, datenum, id, eachRoundBet){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.play(creator, datenum, id, false, {value: util.getPayableValue(eachRoundBet)})
    }
    async foldedPlayers(identifier, players, foldedPlayers){
        const promises = []
        for (const player of players){
            promises.push(this.contract.playersFolded(identifier, player.account))
        }
        const resp = await Promise.all(promises)
        for (let i=0; i<resp.length; i++){
            if (resp[i])
            foldedPlayers.push(players[i].account)
        }
    }
    async askForShow(creator, datenum, id, eachRoundBet){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.play(creator, datenum, id, true, {gasLimit: 200000, value: util.getPayableValue(eachRoundBet*2)})
    }
    async getPlayers(identifier, totalPlayers){
        const promises =[];
        for (let i=0; i<totalPlayers*1; i++){
            promises.push(this.contract.playersInGame(identifier, i))
        }
        const players = await Promise.all(promises)
        return players
    }
    async getRevealedCards(identifier, totalPlayers, revealed){
        const players = await this.getPlayers(identifier, totalPlayers)
        const playersWhoRevealedCards = []
        for (const player of players){
            if (player.revealed) playersWhoRevealedCards.push(player)
        }
        for (const player of playersWhoRevealedCards){
            const currentPlayer = await this.getCurrentAccount()
            if (player.account == currentPlayer) continue;
                let promises = []
                for (let c=0; c<3; c++){
                    promises.push(this.contract.revealedHands(identifier, player.account, c))
                }
                const revealedHand = await Promise.all(promises)
                for (const card of revealedHand){
                    let signature = ranks[card.rank] + suits[card.suit]
                    revealed.push(signature)
                }
        }
    }
    async revealHand(creator, datenum, id){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.revealHand(creator, datenum, id)
    }
    async fold(creator, datenum, id){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.fold(creator, datenum, id)
    }
    async payout(creator, datenum, id){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.payout(creator, datenum, id)
    }
    async startGame(datenum, id){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.startGame(datenum, id);
    }
    async computeWinner(creator, datenum, id){
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.computeWinner(creator, datenum, id); 
    }
    async getHand(suitHash, rankHash){
        let suit, rank
        const account = await this.getCurrentAccount();
        for (let i = 1; i <=4; i++) {
            const hashed = await (ethers.utils.id(i + "" + account))
            if (hashed == suitHash) suit = i
        }
        for (let i = 1; i <=13; i++) {
            const hashed = await (ethers.utils.id(i + "" + account))
            if (hashed == rankHash) rank = i
        }
        return ranks[rank] + suits[suit];
    }
    getCard(suitData, rankData, hash){
        let suit, rank
        for (let i=1; i<14; i++){
            if (soliditySha3(hash, i) == suitData) suit = i;
            if (soliditySha3(hash, i) == rankData) rank = i;
        }
        return ranks[rank] + suits[suit]
    }
    async computeRank(c1r, c1s, c2r, c2s, c3r, c3s){
        return await this.contract.getRank(c1r, c1s, c2r, c2s, c3r, c3s);
    }
    async register(nickname) {
        const account = await this.getCurrentAccount();
        let data = []
        for (let i = 1; i < 14; i++) {
            const hashed = await (ethers.utils.id(i + "" + account))
            data.push(hashed)
        }
        const withSigner = this.contract.connect(provider.getSigner());
        await withSigner.registerUser_1(nickname, data[0], data[1], data[2], data[3], data[4], data[5]);
        setTimeout(async () => {
            await withSigner.registerUser_2(data[6], data[7], data[8], data[9], data[10], data[11], data[12]);
        }, 5000)
        
    }
    async retrieveHand(creator, datenum, id, cards){
        const withSigner = this.contract.connect(provider.getSigner());
        //cards = []
        const handPhrase = Math.floor(Math.random())*1000000 + '';
        const currentAccount = await this.getCurrentAccount();
        const sha3 = soliditySha3(handPhrase + '-' + currentAccount)
        const hash = sha3.substring(2)
        const promises = []
        for (let c=0; c<3; c++){
            promises.push(withSigner.retrieveHand(creator, datenum, id, c, hash))
        }
        const resp = await Promise.all(promises)
        for (let c=0; c<3; c++){
            cards.push(this.getCard(resp[c][0], resp[c][1], hash))
        }
    }
}
module.exports = { CardsClient, suits, ranks, gameStates }