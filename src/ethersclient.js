const ethers = require('ethers');
const util = require('./util');
const BigNumber = require("bignumber.js");

const abi = require('./financialoptions.json')

let provider
class EthersClient{
  isOwner = false;
  
  constructor(){
    this.contract = {}
    this.optionsMap = {}
    this.walletAddr = ''
  }
  async getCurrentAccount(){
    const accounts = await provider.listAccounts();
    return accounts.length > 0 ?  accounts[0] : ''
  }
  async getOwner(){
    return await this.contract.owner();
  }
  async getWalletAddr(){
    return this.walletAddr;
  }
  //Highly inefficient, should be single call
  async getOptions(account, optionType, _optionsMap){
    this.optionsMap = _optionsMap
    const count = await this.getOptionsCount(account)
    for (let i=0; i < count; i++){
      const binaryOption = await this.getOption(account, i)
      if (binaryOption.subjectArea == optionType){// && binaryOption.resolved == false){
        this.optionsMap[binaryOption.identifier] = binaryOption
      }
      
    }
    
  }
  async getOption(account, optionIdx){
    const currentAccount = await this.getCurrentAccount();
    const Option = await this.contract.options(account, optionIdx*1)
    const expiryDateTime = new Date(Option.expiry.toString()*1000);
    const lockDateTime = new Date(Option.lockingTime.toString()*1000);
    const resolved = Option.outcome != 0;
    return {
      ...Option,
      formattedExpiry:util.formatDate(expiryDateTime),
      formattedLocking:util.formatDate(lockDateTime),
      resolved,
      totalAyes: new BigNumber(Option.totalAyes.toString()).shiftedBy(-18),
      totalNoos: new BigNumber(Option.totalNoos.toString()).shiftedBy(-18),
      isOwner: account == currentAccount
    }
    
  }
  /*async processCoupon(Option){
    const yesContractAddr = await this.contract.yesContracts(Option.identifier);
    const noContractAddr = await this.contract.noContracts(Option.identifier);
    if (yesContractAddr == '0x0000000000000000000000000000000000000000' || noContractAddr == '0x0000000000000000000000000000000000000000') return;
    const yesContract = new ethers.Contract(yesContractAddr, erc20abi, provider);
    const noContract = new ethers.Contract(noContractAddr, erc20abi, provider);
    const currentAccount = await this.getCurrentAccount();
    const yesBalanceBN = await yesContract.balanceOf(currentAccount)
    const noBalanceBN = await noContract.balanceOf(currentAccount)
    const yesPrice = await yesContract.price();
    const noPrice = await noContract.price();
    const yesBalance = new BigNumber(yesBalanceBN.toString()).shiftedBy(-18)
    const noBalance  = new BigNumber(noBalanceBN.toString()).shiftedBy(-18)
    if (!yesBalance.isEqualTo(0)) {
      const coupon = this.getCoupon(yesBalance, true, Option, yesContractAddr, yesPrice, noPrice);
      redeemableCoupons.push(coupon);
    } 
    if (!noBalance.isEqualTo(0)) {
      const coupon = this.getCoupon(noBalance, false, Option, noContractAddr, yesPrice, noPrice);
      redeemableCoupons.push(coupon);
    } 
  }
  getCoupon(balance, isYes, Option, contractAddr, yesPrice, noPrice){
      const coupon = {condition: Option.condition}
      coupon.optionId = Option.identifier;
      coupon.value = isYes ? (yesPrice/scaling).toFixed(2) : (noPrice/scaling).toFixed(2);
      coupon.yesNo = isYes ? "Yes" : "No"
      let outcome = ""
      
      switch (Option.outcome) {
        case 0: outcome = "Unresolved"; break;
        case 1: outcome = "Yes"; break;
        case 2: outcome = "No"; break;
      }
      coupon.optionOutcome = outcome
      coupon.redeemable = (isYes && Option.outcome == 1) || (!isYes && Option.outcome == 2) 
      coupon.numUnits = balance.toString();
      const expiryDateTime = new Date(Option.expiry.toString()*1000);
      coupon.redemptionTime = util.formatDate(expiryDateTime);
      coupon.contractAddr = contractAddr;
      return coupon;
  }
  getRedeemableCoupons(){
    return redeemableCoupons;
  }*/
  async getBalance(optionId, yesNo){
    return await this.contract.getResultBalance(optionId, yesNo)
  }
  
  async getOptionsCount(account){
    return await this.contract.getOptionsLength(account)
  }
  async setOptionResult(account, optionId, outcome){
    const withSigner = this.contract.connect(provider.getSigner())
    await withSigner.setOptionResultPrivate(account, optionId, outcome);
  }
  async redeem(account, optionId){
    const withSigner = this.contract.connect(provider.getSigner())
    await withSigner.receivePayment(account, optionId)
  }
  async buyOption(account, identifier, direction, amountToBuy){
    const withSigner = this.contract.connect(provider.getSigner());
    const payableValue = util.getPayableValue(amountToBuy);
    await withSigner.makePrediction(account, identifier, direction, {value: payableValue});
  }
  async createBinaryOption(account, optionType, instr, lockingTime, expiryDateTime, commissionBps){
    const withSigner = this.contract.connect(provider.getSigner());
    if (optionType == 'private'){
      await withSigner.createOptionPrivate(optionType, instr, lockingTime, expiryDateTime, commissionBps).catch(e => console.log(e))
    }
    else await withSigner.createOptionPublic(optionType, instr, lockingTime, expiryDateTime, commissionBps).catch(e => console.log(e))
  }
  /*isOwner(){
    return this.isOwner;
  }*/
  /*async getPastEvents(provider, contract, fromBlock){
    console.log(contract.filters)
    const filter = contract.filters.OptionPlaced(null, null, null, null)
    filter.fromBlock = fromBlock
    filter.toBlock = fromBlock*1 + 100
  
    const logs = await provider.getLogs(filter)
  
    const events = []
    for (let i in logs) {
      events.push(abi.parseLog(logs[i]))
    }
  
    return events
  }*/
  
  async init(){
    provider = new ethers.providers.Web3Provider(window.ethereum);
    const network = await provider.getNetwork()
    if (network.chainId == util.chainId) await this.initContract();
    return network.chainId;
    //this.subscribeToContractEvents();
  }
  async initContract(){
    this.contract = new ethers.Contract(util.contractAddress, abi, provider);
    console.log(this.contract);
    //const ownerAccount =  await this.contract.owner()
    //const currentAccount = await this.getCurrentAccount();
    //this.isOwner = (ownerAccount == currentAccount)
    //this.walletAddr = await this.contract.wallet();
    
  }
  
}

module.exports = {EthersClient}
