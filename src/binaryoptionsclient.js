const ethers = require('ethers');
const abi = require('./binaryoptions.json')

//
class BinaryOptionsClient{
  isOwner = false;
  
  constructor(){
    this.contract = {}
    this.provider = {}
  }
  async getCurrentAccount(){
    const accounts = await this.provider.listAccounts();
    return accounts.length > 0 ?  accounts[0] : ''
  }
  async getContractBalance(){
      return await this.provider.getBalance(this.contract.address);
  }
  async getOwner(){
    return await this.contract.owner();
  }
  
  async init(){
    this.provider = new ethers.providers.Web3Provider(window.ethereum);
    const network = await this.provider.getNetwork()
    await this.initContract();
    return network.chainId;
  }
  async initContract(){
    this.contract = new ethers.Contract('0xc64E5dd346AACEab046a74798A6775Ad044A4c78', abi, this.provider);
  }
}
module.exports = {BinaryOptionsClient}
