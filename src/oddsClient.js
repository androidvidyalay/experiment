const ethers = require('ethers');
//const util = require('./util');
//const BigNumber = require("bignumber.js");

const abi = require('./tradableoptions.json')

//
class OddsClient{
  isOwner = false;
  
  constructor(){
    this.contract = {}
    this.provider = {}
  }
  async getCurrentAccount(){
    const accounts = await this.provider.listAccounts();
    return accounts.length > 0 ?  accounts[0] : ''
  }
  async getContractBalance(){
      return await this.provider.getBalance(this.contract.address);
  }
  async getOwner(){
    return await this.contract.owner();
  }
  async getEvent(eventDateTime, eventName){
    return await this.contract.tradableEvents(eventDateTime, eventName);
  }
  async createEvent(eventDateTime, eventName, eventExpiry, creatorCommission){
    console.log({
      eventDateTime, eventName, eventExpiry, creatorCommission
    })
    const withSigner = this.contract.connect(this.provider.getSigner())  
    await withSigner.createEvent(eventDateTime, eventName, eventExpiry, creatorCommission);

  }
  async createPool(eventDateTime, eventName, yesProbablity, coinsToMint){
    const resp = await this.contract.paymentForDeposit(coinsToMint, yesProbablity)
    const withSigner = this.contract.connect(this.provider.getSigner())  
    await withSigner.createPool(eventDateTime, eventName, yesProbablity, coinsToMint, {value: resp.minDeposit});
    
  }
  async init(){
    this.provider = new ethers.providers.Web3Provider(window.ethereum);
    const network = await this.provider.getNetwork()
    //if (network.chainId == util.chainId) await this.initContract();
    await this.initContract();
    return network.chainId;
  }
  async initContract(){
    this.contract = new ethers.Contract('0xDEC3E2e5be8D4D1B8a2A41d85327F90466802b21', abi, this.provider);
  }
}
module.exports = {OddsClient}
