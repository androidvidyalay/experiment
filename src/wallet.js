const ethers = require('ethers');
const util = require('./util');
const {EthersClient} = require('./ethersclient');
const ethersClient = new EthersClient();
let provider;
let walletAddr = '';
class Wallet {
    async getCurrentAccount(){
        const accounts = await provider.listAccounts();
        return accounts.length > 0 ?  accounts[0] : ''
    }
    async init() {
        provider = new ethers.providers.Web3Provider(window.ethereum);
        const abi = require('./wallet.json')
        walletAddr = await ethersClient.getWalletAddr();
        if (walletAddr == '') {
            await ethersClient.init();
            walletAddr = await ethersClient.getWalletAddr()
            console.log("util wallet addr post init " + walletAddr)
        }
        this.contract = new ethers.Contract(walletAddr, abi, provider)
    }
    async balance(){
        const currentAccount = await this.getCurrentAccount();
        const balance = await this.contract.deposits(currentAccount)
        console.log(balance.toString())
        return balance;
    }
    async withdraw(amount){
        const withSigner = this.contract.connect(provider.getSigner());
        const bn = util.toBNFixed(amount)
        await withSigner.withdraw(bn);
    }
    async deposit(amount){
        const withSigner = this.contract.connect(provider.getSigner());
        const bn = util.toBNFixed(amount)
        await withSigner.deposit(await this.getCurrentAccount(), {value: bn});
    }
    

}
module.exports = { Wallet }

