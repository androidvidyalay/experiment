/**
 * Javascript implementation of the Ranking algo that runs on the blockchain.
 * @param {*} cards - array of 3 card object. Each Card object is {suit: <1,2,3,4>, rank: <1-13>}. Note it's not Zero based index
 * @param {*} countMap - you can create an array of cards, and call this function with countMap. Finally, you will get group wise count. e.g. 4 Trails, 5 Sequences and so on
 * @returns 
 */
const getScore = function(cards, countMap){
  if (!countMap)
  countMap = {'trail': 0, 'sequence': 0, 'pair':0, 'color': 0, 'highest': 0}

    cards.sort((a, b) =>  (b.rank - a.rank));

    let score = 0;
    let index = 1;
  //Colors
  if (cards[0].suit == cards[1].suit && cards[1].suit == cards[2].suit) {
    countMap['color'] =  countMap['color'] + 1;
    score = 10 ** 6;
  }
  //Trail or triple, AAA KKK 222 and so on
  if (cards[0].rank == cards[1].rank && cards[1].rank == cards[2].rank){
    countMap['trail'] =  countMap['trail'] + 1;
    score +=
      10**8 +
      (2 ** cards[0].rank) +
      (2 ** cards[1].rank) +
      (2 ** cards[2].rank);
  }
  //Sequence
  else if (cards[0].rank - cards[1].rank == 1 && cards[1].rank - cards[2].rank == 1){
    countMap['sequence'] =  countMap['sequence'] + 1;
    score +=
      (10 ** 7) +
      (2 ** cards[0].rank) +
      (2 ** cards[1].rank) +
      (2 ** cards[2].rank);
  }
  //Double
  else if (cards[0].rank == cards[1].rank || cards[1].rank == cards[2].rank) {
    countMap['pair'] =  countMap['pair'] + 1;
    if (cards[1].rank == 1) {
      index = 14;
    } else index = cards[1].rank;
    score +=
      (10 ** 5) +
      (2 ** cards[0].rank) +
      (2 ** cards[1].rank) +
      (2 ** cards[2].rank) +
      16338 * index;
  } //Nothing
  else{
    countMap['highest'] =  countMap['highest'] + 1;
    score +=
      (2 ** cards[0].rank) +
      (2 ** cards[1].rank) +
      (2 ** cards[2].rank);
  }
  //This condtion for RKA
  if (cards[0].rank == 13 && cards[1].rank == 12 && cards[2].rank == 1)
    score +=
      (10 ** 7) +
      (2 ** cards[0].rank) +
      (2 ** cards[1].rank) +
      (2 ** cards[2].rank);
  if (cards[2].rank == 1) score += 24572;

return score;

}
module.exports = {
  getScore
}

